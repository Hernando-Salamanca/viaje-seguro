<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\EstadoPasajero;
use app\models\EstadoPasajeroSearch;
use app\models\Pasajero;
use Codeception\PHPUnit\Constraint\Page;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['consulta']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionConsulta()
    {
        $pasajero = new Pasajero();
        $estado_pasajero = new EstadoPasajero();
        if ($pasajero->load(Yii::$app->request->post())) {
            if ($consulta = Pasajero::find()->where(['documento_pasajero' => $pasajero->documento_pasajero])->one()) {
                $estado_pasajero->id_pasajero = $consulta->documento_pasajero;
                return $this->redirect(['/estadopasajero/create', 'id' => $consulta->id_pasajero]);
            } else {
                return $this->redirect(['/pasajero/create', 'id' => $pasajero->documento_pasajero]);
            }
        }
        if ($estado_pasajero->load(Yii::$app->request->post())) {
        }
        return $this->render('consulta', ['pasajero' => $pasajero]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionConsulta_pasajero()
    {
        $pasajero = new Pasajero();
        if ($pasajero->load(Yii::$app->request->post())) {
            if ($consulta = Pasajero::find()->where(['documento_pasajero' => $pasajero->documento_pasajero])->one()) {
                return $this->redirect(['/estadopasajero/index_pasajero', 'id' => $consulta->id_pasajero]);
                //return $this->render('view_pasajero', ['estado_pasajeros' => $estado_pasajero]);
            } else {
                Yii::$app->session->setFlash('danger', "Usuario no registrado en nuestra plataforma");
            }
        }

        return $this->render('consulta_pasajero', ['pasajero' => $pasajero]);
    }
}
