-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 07-11-2020 a las 02:56:02
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `pintegrador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conductor`
--

CREATE TABLE `conductor` (
  `id_conductor` int(11) NOT NULL,
  `nombre_conductor` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_conductor` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `documento_conductor` double NOT NULL,
  `numero_licencia` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `conductor`
--

INSERT INTO `conductor` (`id_conductor`, `nombre_conductor`, `apellido_conductor`, `documento_conductor`, `numero_licencia`) VALUES
(1, 'Duvan ', 'Cardenas', 1070964543, 76454674);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_pasajero`
--

CREATE TABLE `estado_pasajero` (
  `id_estado_pasajero` int(11) NOT NULL,
  `temperatura` double NOT NULL,
  `sintomas_asociados` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `familiares_contagiados` int(11) NOT NULL,
  `id_pasajero` int(11) NOT NULL,
  `id_ruta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasajero`
--

CREATE TABLE `pasajero` (
  `id_pasajero` int(11) NOT NULL,
  `nombre_pasajero` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_pasajero` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `documento_pasajero` double NOT NULL,
  `tipo_documento_pasajero` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pasajero`
--

INSERT INTO `pasajero` (`id_pasajero`, `nombre_pasajero`, `apellido_pasajero`, `documento_pasajero`, `tipo_documento_pasajero`) VALUES
(1, 'Duvan', 'Cardenas Herrera', 1098746534, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ruta`
--

CREATE TABLE `ruta` (
  `id_ruta` int(11) NOT NULL,
  `num_vehiculo` int(11) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `lugar_salida` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `lugar_llegada` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `id_conductor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ruta_pasajero`
--

CREATE TABLE `ruta_pasajero` (
  `id_ruta_pasajero` int(11) NOT NULL,
  `id_pasajero` int(11) NOT NULL,
  `id_ruta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', 'P7XPckF-9InTiUgwxl79aWWXUUjXwmj-', '$2y$13$F.EehtdCQ6X/zedcjg6Ml.7tjqC3ut.hYJ2/QIj10h/tpdXcdcIFq', NULL, 'info@legift.co', 10, 1583538933, 1583538957, 'B5LSdVlT-RWdMYOYi_rPklqH2A5xlPzc_1583538933');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `conductor`
--
ALTER TABLE `conductor`
  ADD PRIMARY KEY (`id_conductor`),
  ADD KEY `id_conuctor` (`id_conductor`);

--
-- Indices de la tabla `estado_pasajero`
--
ALTER TABLE `estado_pasajero`
  ADD PRIMARY KEY (`id_estado_pasajero`),
  ADD KEY `id_pasajero` (`id_pasajero`),
  ADD KEY `id_ruta` (`id_ruta`);

--
-- Indices de la tabla `pasajero`
--
ALTER TABLE `pasajero`
  ADD PRIMARY KEY (`id_pasajero`);

--
-- Indices de la tabla `ruta`
--
ALTER TABLE `ruta`
  ADD PRIMARY KEY (`id_ruta`),
  ADD KEY `id_conductor` (`id_conductor`);

--
-- Indices de la tabla `ruta_pasajero`
--
ALTER TABLE `ruta_pasajero`
  ADD PRIMARY KEY (`id_ruta_pasajero`),
  ADD KEY `id_pasajero` (`id_pasajero`),
  ADD KEY `id_ruta` (`id_ruta`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `conductor`
--
ALTER TABLE `conductor`
  MODIFY `id_conductor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `estado_pasajero`
--
ALTER TABLE `estado_pasajero`
  MODIFY `id_estado_pasajero` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pasajero`
--
ALTER TABLE `pasajero`
  MODIFY `id_pasajero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ruta`
--
ALTER TABLE `ruta`
  MODIFY `id_ruta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ruta_pasajero`
--
ALTER TABLE `ruta_pasajero`
  MODIFY `id_ruta_pasajero` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `estado_pasajero`
--
ALTER TABLE `estado_pasajero`
  ADD CONSTRAINT `estado_pasajero_ibfk_1` FOREIGN KEY (`id_pasajero`) REFERENCES `pasajero` (`id_pasajero`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `estado_pasajero_ibfk_2` FOREIGN KEY (`id_ruta`) REFERENCES `ruta` (`id_ruta`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ruta`
--
ALTER TABLE `ruta`
  ADD CONSTRAINT `ruta_ibfk_1` FOREIGN KEY (`id_conductor`) REFERENCES `conductor` (`id_conductor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ruta_pasajero`
--
ALTER TABLE `ruta_pasajero`
  ADD CONSTRAINT `ruta_pasajero_ibfk_1` FOREIGN KEY (`id_pasajero`) REFERENCES `pasajero` (`id_pasajero`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ruta_pasajero_ibfk_2` FOREIGN KEY (`id_ruta`) REFERENCES `ruta` (`id_ruta`) ON DELETE CASCADE ON UPDATE CASCADE;
