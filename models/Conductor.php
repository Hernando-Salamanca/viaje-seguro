<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conductor".
 *
 * @property int $id_conductor
 * @property string $nombre_conductor
 * @property string $apellido_conductor
 * @property float $documento_conductor
 * @property float $numero_licencia
 *
 * @property Ruta[] $rutas
 */
class Conductor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conductor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_conductor', 'apellido_conductor', 'documento_conductor', 'numero_licencia'], 'required'],
            [['documento_conductor', 'numero_licencia'], 'number'],
            [['nombre_conductor', 'apellido_conductor'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_conductor' => 'Id Conductor',
            'nombre_conductor' => 'Nombre Conductor',
            'apellido_conductor' => 'Apellido Conductor',
            'documento_conductor' => 'Documento Conductor',
            'numero_licencia' => 'Numero Licencia',
        ];
    }

    /**
     * Gets query for [[Rutas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRutas()
    {
        return $this->hasMany(Ruta::className(), ['id_conductor' => 'id_conductor']);
    }
}
