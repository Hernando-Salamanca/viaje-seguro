<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Conductor;

/**
 * ConductorSearch represents the model behind the search form of `app\models\Conductor`.
 */
class ConductorSearch extends Conductor
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_conductor'], 'integer'],
            [['nombre_conductor', 'apellido_conductor'], 'safe'],
            [['documento_conductor', 'numero_licencia'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Conductor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_conductor' => $this->id_conductor,
            'documento_conductor' => $this->documento_conductor,
            'numero_licencia' => $this->numero_licencia,
        ]);

        $query->andFilterWhere(['like', 'nombre_conductor', $this->nombre_conductor])
            ->andFilterWhere(['like', 'apellido_conductor', $this->apellido_conductor]);

        return $dataProvider;
    }
}
