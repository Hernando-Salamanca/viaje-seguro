<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "estado_pasajero".
 *
 * @property int $id_estado_pasajero
 * @property float $temperatura
 * @property string $sintomas_asociados
 * @property int $familiares_contagiados
 * @property int $id_pasajero
 * @property int $id_ruta
 *
 * @property Pasajero $pasajero
 * @property Ruta $ruta
 */
class EstadoPasajero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estado_pasajero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['temperatura', 'sintomas_asociados', 'familiares_contagiados', 'id_pasajero', 'id_ruta'], 'required'],
            [['temperatura'], 'number'],
            [['familiares_contagiados', 'id_pasajero', 'id_ruta'], 'integer'],
            [['sintomas_asociados'], 'string', 'max' => 100],
            [['id_pasajero'], 'exist', 'skipOnError' => true, 'targetClass' => Pasajero::className(), 'targetAttribute' => ['id_pasajero' => 'id_pasajero']],
            [['id_ruta'], 'exist', 'skipOnError' => true, 'targetClass' => Ruta::className(), 'targetAttribute' => ['id_ruta' => 'id_ruta']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_estado_pasajero' => 'Codigo Estado Pasajero',
            'temperatura' => 'Temperatura',
            'sintomas_asociados' => 'Sintomas Asociados',
            'familiares_contagiados' => 'Familiares Contagiados',
            'id_pasajero' => 'Pasajero',
            'id_ruta' => 'Ruta',
        ];
    }

    /**
     * Gets query for [[Pasajero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPasajero()
    {
        return $this->hasOne(Pasajero::className(), ['id_pasajero' => 'id_pasajero']);
    }

    /**
     * Gets query for [[Ruta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRuta()
    {
        return $this->hasOne(Ruta::className(), ['id_ruta' => 'id_ruta']);
    }
}
