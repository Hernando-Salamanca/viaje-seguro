<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EstadoPasajero;

/**
 * EstadoPasajeroSearch represents the model behind the search form of `app\models\EstadoPasajero`.
 */
class EstadoPasajeroSearch extends EstadoPasajero
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_estado_pasajero', 'familiares_contagiados', 'id_pasajero', 'id_ruta'], 'integer'],
            [['temperatura'], 'number'],
            [['sintomas_asociados'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (array_key_exists('id', $params)) {
            $query = EstadoPasajero::find()->where(['id_pasajero' => $params['id']]);
        } else {
            $query = EstadoPasajero::find();
        }
        //die();



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_estado_pasajero' => $this->id_estado_pasajero,
            'temperatura' => $this->temperatura,
            'familiares_contagiados' => $this->familiares_contagiados,
            'id_pasajero' => $this->id_pasajero,
            'id_ruta' => $this->id_ruta,
        ]);

        $query->andFilterWhere(['like', 'sintomas_asociados', $this->sintomas_asociados]);

        return $dataProvider;
    }
}
