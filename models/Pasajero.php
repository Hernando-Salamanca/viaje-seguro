<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pasajero".
 *
 * @property int $id_pasajero
 * @property string $nombre_pasajero
 * @property string $apellido_pasajero
 * @property float $documento_pasajero
 * @property string $tipo_documento_pasajero
 *
 * @property EstadoPasajero[] $estadoPasajeros
 * @property RutaPasajero[] $rutaPasajeros
 */
class Pasajero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasajero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_pasajero', 'apellido_pasajero', 'documento_pasajero', 'tipo_documento_pasajero'], 'required'],
            [['documento_pasajero'], 'number'],
            [['nombre_pasajero', 'apellido_pasajero', 'tipo_documento_pasajero'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pasajero' => 'Codigo Pasajero',
            'nombre_pasajero' => 'Nombre Pasajero',
            'apellido_pasajero' => 'Apellido Pasajero',
            'documento_pasajero' => 'Documento Pasajero',
            'tipo_documento_pasajero' => 'Tipo Documento Pasajero',
        ];
    }

    /**
     * Gets query for [[EstadoPasajeros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoPasajeros()
    {
        return $this->hasMany(EstadoPasajero::className(), ['id_pasajero' => 'id_pasajero']);
    }

    /**
     * Gets query for [[RutaPasajeros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRutaPasajeros()
    {
        return $this->hasMany(RutaPasajero::className(), ['id_pasajero' => 'id_pasajero']);
    }
}
