<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pasajero;

/**
 * PasajeroSearch represents the model behind the search form of `app\models\Pasajero`.
 */
class PasajeroSearch extends Pasajero
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pasajero'], 'integer'],
            [['nombre_pasajero', 'apellido_pasajero', 'tipo_documento_pasajero'], 'safe'],
            [['documento_pasajero'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pasajero::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pasajero' => $this->id_pasajero,
            'documento_pasajero' => $this->documento_pasajero,
        ]);

        $query->andFilterWhere(['like', 'nombre_pasajero', $this->nombre_pasajero])
            ->andFilterWhere(['like', 'apellido_pasajero', $this->apellido_pasajero])
            ->andFilterWhere(['like', 'tipo_documento_pasajero', $this->tipo_documento_pasajero]);

        return $dataProvider;
    }
}
