<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ruta".
 *
 * @property int $id_ruta
 * @property int $num_vehiculo
 * @property int $capacidad
 * @property string $fecha_hora
 * @property string $lugar_salida
 * @property string $lugar_llegada
 * @property int $id_conductor
 *
 * @property EstadoPasajero[] $estadoPasajeros
 * @property Conductor $conductor
 * @property RutaPasajero[] $rutaPasajeros
 */
class Ruta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ruta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_vehiculo', 'capacidad', 'fecha_hora', 'lugar_salida', 'lugar_llegada', 'id_conductor'], 'required'],
            [['num_vehiculo', 'capacidad', 'id_conductor'], 'integer'],
            [['fecha_hora'], 'safe'],
            [['lugar_salida', 'lugar_llegada'], 'string', 'max' => 100],
            [['id_conductor'], 'exist', 'skipOnError' => true, 'targetClass' => Conductor::className(), 'targetAttribute' => ['id_conductor' => 'id_conductor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ruta' => 'Codigo Ruta',
            'num_vehiculo' => 'Numero de Vehiculo',
            'capacidad' => 'Capacidad',
            'fecha_hora' => 'Fecha y Hora',
            'lugar_salida' => 'Lugar de Salida',
            'lugar_llegada' => 'Lugar de Llegada',
            'id_conductor' => 'Conductor',
        ];
    }

    /**
     * Gets query for [[EstadoPasajeros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoPasajeros()
    {
        return $this->hasMany(EstadoPasajero::className(), ['id_ruta' => 'id_ruta']);
    }

    /**
     * Gets query for [[Conductor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConductor()
    {
        return $this->hasOne(Conductor::className(), ['id_conductor' => 'id_conductor']);
    }

    /**
     * Gets query for [[RutaPasajeros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRutaPasajeros()
    {
        return $this->hasMany(RutaPasajero::className(), ['id_ruta' => 'id_ruta']);
    }
}
