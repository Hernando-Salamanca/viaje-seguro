<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ruta_pasajero".
 *
 * @property int $id_ruta_pasajero
 * @property int $id_pasajero
 * @property int $id_ruta
 *
 * @property Pasajero $pasajero
 * @property Ruta $ruta
 */
class RutaPasajero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ruta_pasajero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pasajero', 'id_ruta'], 'required'],
            [['id_pasajero', 'id_ruta'], 'integer'],
            [['id_pasajero'], 'exist', 'skipOnError' => true, 'targetClass' => Pasajero::className(), 'targetAttribute' => ['id_pasajero' => 'id_pasajero']],
            [['id_ruta'], 'exist', 'skipOnError' => true, 'targetClass' => Ruta::className(), 'targetAttribute' => ['id_ruta' => 'id_ruta']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ruta_pasajero' => 'Id Ruta Pasajero',
            'id_pasajero' => 'Id Pasajero',
            'id_ruta' => 'Id Ruta',
        ];
    }

    /**
     * Gets query for [[Pasajero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPasajero()
    {
        return $this->hasOne(Pasajero::className(), ['id_pasajero' => 'id_pasajero']);
    }

    /**
     * Gets query for [[Ruta]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRuta()
    {
        return $this->hasOne(Ruta::className(), ['id_ruta' => 'id_ruta']);
    }
}
