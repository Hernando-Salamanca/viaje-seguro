<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ruta;

/**
 * RutaSearch represents the model behind the search form of `app\models\Ruta`.
 */
class RutaSearch extends Ruta
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ruta', 'num_vehiculo', 'capacidad', 'id_conductor'], 'integer'],
            [['fecha_hora', 'lugar_salida', 'lugar_llegada'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ruta::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_ruta' => $this->id_ruta,
            'num_vehiculo' => $this->num_vehiculo,
            'capacidad' => $this->capacidad,
            'fecha_hora' => $this->fecha_hora,
            'id_conductor' => $this->id_conductor,
        ]);

        $query->andFilterWhere(['like', 'lugar_salida', $this->lugar_salida])
            ->andFilterWhere(['like', 'lugar_llegada', $this->lugar_llegada]);

        return $dataProvider;
    }
}
