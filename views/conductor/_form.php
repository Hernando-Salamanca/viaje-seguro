<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Conductor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conductor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_conductor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido_conductor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'documento_conductor')->textInput() ?>

    <?= $form->field($model, 'numero_licencia')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
