<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ConductorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conductor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_conductor') ?>

    <?= $form->field($model, 'nombre_conductor') ?>

    <?= $form->field($model, 'apellido_conductor') ?>

    <?= $form->field($model, 'documento_conductor') ?>

    <?= $form->field($model, 'numero_licencia') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
