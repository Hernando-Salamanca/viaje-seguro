<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConductorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conductores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conductor-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Conductor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_conductor',
            'nombre_conductor',
            'apellido_conductor',
            'documento_conductor',
            'numero_licencia',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
