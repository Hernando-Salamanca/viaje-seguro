<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conductor */

$this->title = 'Actualizar Conductor: ' . $model->id_conductor;
$this->params['breadcrumbs'][] = ['label' => 'Conductors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_conductor, 'url' => ['view', 'id' => $model->id_conductor]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="conductor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
