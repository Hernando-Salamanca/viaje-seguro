<?php

use app\models\Ruta;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EstadoPasajero */
/* @var $form yii\widgets\ActiveForm */

$request = Yii::$app->request;
$id = $request->get('id');
?>

<div class="estado-pasajero-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'temperatura')->textInput() ?>

    <?= $form->field($model, 'sintomas_asociados')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'familiares_contagiados')->textInput() ?>

    <?= $form->field($model, 'id_pasajero')->hiddenInput(['value' => $id])->label(false) ?>

    <?= $form->field($model, 'id_ruta')->dropDownList(
        ArrayHelper::map(
            \app\models\Ruta::find()->asArray()->orderBy([
                'id_ruta' => SORT_DESC,
            ])->all(),
            'id_ruta',
            function ($model) {
                return $model['id_ruta'] . ' - ' . $model['lugar_salida'] . ' - ' . $model['lugar_llegada'];
            }
        )
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>