<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EstadoPasajeroSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estado-pasajero-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_estado_pasajero') ?>

    <?= $form->field($model, 'temperatura') ?>

    <?= $form->field($model, 'sintomas_asociados') ?>

    <?= $form->field($model, 'familiares_contagiados') ?>

    <?= $form->field($model, 'id_pasajero') ?>

    <?php // echo $form->field($model, 'id_ruta') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
