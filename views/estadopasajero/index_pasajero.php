<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EstadoPasajeroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Estado Pasajeros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estado-pasajero-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id_estado_pasajero',

            [
                'attribute' => 'id_ruta',
                'label' => 'Ruta',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->id_ruta, ['ruta/view', 'id' => $data->id_ruta]);
                },
            ],

            // [
            //     'attribute' => 'nombre_pasajero',
            //     'label' => 'Nombre',
            //     'format' => 'raw',
            //     'value' => function ($data) {
            //         return Html::a($data->nombre_pasajero, ['pasajero/view', 'id' => $data->nombre_pasajero]);
            //     },
            // ],

            // 'nombre',
            'temperatura',
            'sintomas_asociados',
            'familiares_contagiados',
            //'id_pasajero',
            //'id_ruta',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>