<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EstadoPasajero */

$this->title = 'Actualizar Estado Pasajero: ' . $model->id_estado_pasajero;
$this->params['breadcrumbs'][] = ['label' => 'Estado Pasajeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_estado_pasajero, 'url' => ['view', 'id' => $model->id_estado_pasajero]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estado-pasajero-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
