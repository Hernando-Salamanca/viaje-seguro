<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pasajero */
/* @var $form yii\widgets\ActiveForm */

$request = Yii::$app->request;
$id = $request->get('id');
?>

<div class="pasajero-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_pasajero')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido_pasajero')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'documento_pasajero')->hiddenInput(['value' => $id])->label(false) ?>

    <?php 
        $var = [ 'Cedula de Ciudadania' => 'Cedula de Ciudadania', 'Tarjeta de Identidad' => 'Tarjeta de Identidad'];

        echo $form->field($model, 'tipo_documento_pasajero')->dropDownList($var, ['prompt' => 'Seleccione Tipo de Documento' ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>