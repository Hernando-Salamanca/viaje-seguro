<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PasajeroSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pasajero-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_pasajero') ?>

    <?= $form->field($model, 'nombre_pasajero') ?>

    <?= $form->field($model, 'apellido_pasajero') ?>

    <?= $form->field($model, 'documento_pasajero') ?>

    <?= $form->field($model, 'tipo_documento_pasajero') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
