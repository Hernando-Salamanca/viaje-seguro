<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pasajero */

$this->title = 'Crear Pasajero';
$this->params['breadcrumbs'][] = ['label' => 'Pasajeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pasajero-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
