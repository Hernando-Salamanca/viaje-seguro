<?php

use app\models\Conductor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ruta */
/* @var $form yii\widgets\ActiveForm */

$hoy = date("Y-m-d h:i:s");
?>

<div class="ruta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fecha_hora')->textInput(['readOnly' => true, 'value' => $hoy]) ?>

    <?= $form->field($model, 'num_vehiculo')->textInput() ?>

    <?= $form->field($model, 'capacidad')->textInput(['value' => 19]) ?>

    <?= $form->field($model, 'lugar_salida')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lugar_llegada')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'id_conductor')->dropDownList(
            ArrayHelper::map(
                \app\models\Conductor::find()->asArray()->all(),
                'id_conductor',
                function($model) {
                    return $model['nombre_conductor'].' '.$model['apellido_conductor'];
                }
            )
        ) ?>    

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>