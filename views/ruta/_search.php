<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RutaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ruta-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_ruta') ?>

    <?= $form->field($model, 'num_vehiculo') ?>

    <?= $form->field($model, 'capacidad') ?>

    <?= $form->field($model, 'fecha_hora') ?>

    <?= $form->field($model, 'lugar_salida') ?>

    <?php // echo $form->field($model, 'lugar_llegada') ?>

    <?php // echo $form->field($model, 'id_conductor') ?>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Eliminar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
