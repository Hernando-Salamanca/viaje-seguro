<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RutaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rutas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ruta-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Ruta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_ruta',
            'num_vehiculo',
            'capacidad',
            'fecha_hora',
            'lugar_salida',
            //'lugar_llegada',
            //'id_conductor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
