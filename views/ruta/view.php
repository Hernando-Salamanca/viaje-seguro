<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ruta */

$this->title = 'Ruta # ' . $model->id_ruta . ' ' . $model->lugar_salida . ' - ' . $model->lugar_llegada;
$this->params['breadcrumbs'][] = ['label' => 'Rutas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ruta-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if (!Yii::$app->user->isGuest) {
        ?>
            <?= Html::a('Actualizar', ['update', 'id' => $model->id_ruta], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Eliminar', ['delete', 'id' => $model->id_ruta], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Esta seguro que desea eliminar este item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_ruta',
            'num_vehiculo',
            'capacidad',
            'fecha_hora',
            'lugar_salida',
            'lugar_llegada',
            'conductor.nombre_conductor',
        ],
    ]) ?>

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Temperatura</th>
                <th>Sintomas</th>
                <th>Familiares contagiados</th>

            </tr>
        </thead>
        <tbody>
            <?php
            $cont = 1;
            foreach ($estado_pasajeros as $estado_pasajero) {
            ?>
                <tr>
                    <td><?= $cont ?></td>
                    <td><?= $estado_pasajero->pasajero->nombre_pasajero ?></td>
                    <td><?= $estado_pasajero->pasajero->apellido_pasajero ?></td>
                    <td><?= $estado_pasajero->temperatura ?></td>
                    <td><?= $estado_pasajero->sintomas_asociados ?></td>
                    <td><?= $estado_pasajero->familiares_contagiados ?></td>
                </tr>
            <?php
                $cont++;
            }
            ?>
        </tbody>
    </table>

</div>