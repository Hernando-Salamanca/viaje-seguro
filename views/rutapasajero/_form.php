<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RutaPasajero */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ruta-pasajero-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_pasajero')->textInput() ?>

    <?= $form->field($model, 'id_ruta')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
