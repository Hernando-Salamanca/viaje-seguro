<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RutaPasajero */

$this->title = 'Crear Ruta Pasajero';
$this->params['breadcrumbs'][] = ['label' => 'Ruta Pasajeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ruta-pasajero-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
