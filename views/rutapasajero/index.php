<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RutaPasajeroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ruta Pasajeros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ruta-pasajero-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Ruta Pasajero', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_ruta_pasajero',
            'id_pasajero',
            'id_ruta',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
