<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RutaPasajero */

$this->title = 'Actualizar Ruta Pasajero: ' . $model->id_ruta_pasajero;
$this->params['breadcrumbs'][] = ['label' => 'Ruta Pasajeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_ruta_pasajero, 'url' => ['view', 'id' => $model->id_ruta_pasajero]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ruta-pasajero-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
