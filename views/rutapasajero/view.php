<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RutaPasajero */

$this->title = $model->id_ruta_pasajero;
$this->params['breadcrumbs'][] = ['label' => 'Ruta Pasajeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ruta-pasajero-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['Actualizar', 'id' => $model->id_ruta_pasajero], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['Eliminar', 'id' => $model->id_ruta_pasajero], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Esta seguro que desea eliminar este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_ruta_pasajero',
            'id_pasajero',
            'id_ruta',
        ],
    ]) ?>

</div>
