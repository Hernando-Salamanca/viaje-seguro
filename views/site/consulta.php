<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pasajero */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pasajero-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($pasajero, 'documento_pasajero')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>